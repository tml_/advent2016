#!/usr/bin/python

import fileinput, sys

def count_bits(i):
    bit = 1
    count = 0
    while bit <= i:
        if bit & i == bit: 
            count += 1
        bit *= 2
    return count

def system(x, y):
    return x*x + 3*x + 2*x*y + y + y*y

def is_wall(x, y, seed):
    if x < 0 or y < 0: return True
    return count_bits(system(x, y) + seed) % 2 == 1

def step(paths, path, x, y):
    if not is_wall(x, y, seed):
        if [x, y] not in path:
            paths.append(path + [[x,y]])

# Part 1: What is the fewest number of steps required for you to reach x,y?
def goal_walk(paths, to_x, to_y, seed):
    while True:
        path = paths.pop(0)
        curr_x, curr_y = path[-1][0], path[-1][1]
        if curr_x == to_x and curr_y == to_y:
            return len(path) - 1
        else:
            step(paths, path, curr_x-1, curr_y)
            step(paths, path, curr_x+1, curr_y)
            step(paths, path, curr_x, curr_y-1)
            step(paths, path, curr_x, curr_y+1)

# Part 2: How many locations (distinct x,y coordinates, including your starting location) can you reach in at most N steps?
def visit_walk(paths, n, seed):
    visited = []
    while True:
        path = paths.pop(0)
        curr_x, curr_y = path[-1][0], path[-1][1]
        if len(path) == n+2:
            return len(visited)
        else:
            if [curr_x, curr_y] not in visited: visited.append([curr_x, curr_y])
            step(paths, path, curr_x-1, curr_y)
            step(paths, path, curr_x+1, curr_y)
            step(paths, path, curr_x, curr_y-1)
            step(paths, path, curr_x, curr_y+1)

def create_room(seed, path):
    layout = ""
    x = max(p[0] for p in path)
    y = max(p[1] for p in path)
    for i in range(y+1):
        for j in range(x+1):
            if [x, y] in path:
                layout += "O"
            elif is_wall(j, i, seed):
                layout += "#"
            else:
                layout += "."
        layout += "\n"
    return layout

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print "USAGE %s width height seed n"
    else:
        x, y, seed, n = int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4])
        print "Part 1:", goal_walk([[[1,1]]], x, y, seed)
        print "Part 2:", visit_walk([[[1,1]]], n, seed)
