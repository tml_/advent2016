#!/usr/bin/python

import fileinput

def parse(s):
    regular = []
    hypernet = []
    state = ''
    token = ""
    for letter in s:
        if letter == '[' and state == '':
            if len(token) > 0:
                regular += aba(token)
                token = ""
            state = '['
        elif letter == ']' and state == '[':
            if len(token) > 0:
                hypernet += aba(token)
                token = ""
            state = ''
        elif letter != '[' and letter != ']':
            token += letter
        else:
            print "Parser failure", s, regular, hypernet, token, state, letter
            assert(False)
    if len(token) > 0 and state == '':
        regular += aba(token)
        token = ""
    else:
        print "Parser failure", s, regular, hypernet, token, state
        assert(False)
    return regular, hypernet

def aba(s):
    matches = []
    for i in range(len(s) - 2):
        sample = s[i:i+3]
        if sample[0] != sample[1] and sample[0] == sample[2]:
            matches += [sample]
    return matches

assert(aba("aaa") == [])
assert(aba("aba") == ["aba"])
assert(aba("zazbz") == ["zaz", "zbz"])

def flip(s):
    assert(len(s) == 3 and s[0] == s[2])
    return s[1] + s[0] + s[1]

assert(flip("aba") == "bab")

def run(line):
    regular, hypernet = parse(line)
    return len(set(regular).intersection(flip(section) for section in hypernet)) > 0

assert(run("aba[bab]xyz") == True)
assert(run("xyx[xyx]xyx") == False)
assert(run("aaa[kek]eke") == True)
assert(run("zazbz[bzb]cdb") == True)

if __name__ == "__main__":
    print sum(1 for line in fileinput.input() if run(line.strip()))
