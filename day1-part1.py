#!/usr/bin/python

import fileinput

left = {"N":"W", "E":"N", "S":"E", "W":"S"}
right = {"N":"E", "E":"S", "S":"W", "W":"N"}
vectors = {"N":(0,1), "E":(1,0), "S":(0,-1), "W":(-1,0)}

def move(s, position, direction):
    if s[0] == "L":
        direction = left[direction]
    elif s[0] == "R":
        direction = right[direction]
    else:
        print "ERROR: Unhandled %s" % (s)
    blocks = int(s[1:])
    position = [position[0]+vectors[direction][0]*blocks, position[1]+vectors[direction][1]*blocks]
    return (position, direction)

def run(s, position, direction):
    codes = s.split(", ")
    for code in codes:
        position, direction = move(code, position, direction)
    return abs(position[0]) + abs(position[1])

if __name__ == "__main__":
    for line in fileinput.input():
        print run(line, [0,0], "N")
        