#!/usr/bin/python

import collections

def one_round(elves):
    if len(elves) % 2 == 0: # Even
        return [elves[i] for i in range(len(elves)) if i % 2 == 0]
    else:
        return [elves[i] for i in range(len(elves)) if i % 2 == 0 and i != 0]

assert(one_round([1, 2, 3, 4, 5]) == [3, 5])
assert(one_round([3, 5]) == [3])

def white_elephant_p1(n):
    elves = [i+1 for i in range(n)]
    while len(elves) > 1:
        elves = one_round(elves)
    return elves[0]

assert(white_elephant_p1(5) == 3)

class Node:
    def __init__(self, value):
        self.value = value
        self.prev = None
        self.next = None
    def __str__(self):
        return "Node(%s, prev: %s, next: %s)" % (self.value, self.prev.value, self.next.value)
    def delete(self):
        self.prev.next, self.next.prev = self.next, self.prev

def circular_list(values):
    l = map(Node, values)
    for i in range(len(l)):
        l[i].prev = l[i-1]
        l[i].next = l[(i+1)%len(l)]
    return l

def white_elephant_p2(n):
    elves = circular_list([i+1 for i in range(n)])
    current = elves[0]
    mid = elves[n/2]
    for i in range(n-1):
        mid.delete()
        if (n - i) % 2 == 0:
            mid = mid.next
        else:
            mid = mid.next.next
        current = current.next
    return current.value

assert(white_elephant_p2(5) == 2)

if __name__ == "__main__":
    puzzle = 3014387
    print "Puzzle start"
    print "Part 1:", white_elephant_p1(puzzle)
    print "Part 2:", white_elephant_p2(puzzle)


