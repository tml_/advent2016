#!/usr/bin/python

def dragon(s):
    return s + "0" + "".join(reversed(["1" if l == "0" else "0" for l in s]))

assert(dragon("0") == "001")
assert(dragon("1") == "100")
assert(dragon("11111") == "11111000000")
assert(dragon("111100001010") == "1111000010100101011110000")

def checksum(s):
    if len(s) % 2 == 1: return s
    return checksum("".join("1" if s[i] == s[i+1] else "0" for i in range(0, len(s), 2)))

assert(checksum("110010110100") == "100")

def dragon_data(s, n):
    if len(s) > n: return checksum(s[:n])
    return dragon_data(dragon(s), n)

assert(dragon_data("10000", 20) == "01100")

if __name__ == "__main__":
    print "Part 1:", dragon_data("10010000000110000", 272)
    print "Part 2:", dragon_data("10010000000110000", 35651584)


