#!/usr/bin/python

import fileinput

def parse(s):
    regular = []
    hypernet = []
    state = ''
    token = ""
    for letter in s:
        if letter == '[' and state == '':
            if len(token) > 0:
                regular += [token]
                token = ""
            state = '['
        elif letter == ']' and state == '[':
            if len(token) > 0:
                hypernet += [token]
                token = ""
            state = ''
        elif letter != '[' and letter != ']':
            token += letter
        else:
            print "Parser failure", s, regular, hypernet, token, state, letter
            assert(False)
    if len(token) > 0 and state == '':
        regular += [token]
        token = ""
    else:
        print "Parser failure", s, regular, hypernet, token, state
        assert(False)
    return regular, hypernet

def test(s):
    if len(s) < 4: return False
    for i in range(len(s) - 3):
        sample = s[i:i+4]
        if sample[0] == sample[3] and sample[1] == sample[2] and sample[0] != sample[1]:
            return True
    return False

def run(line):
    regular, hypernet = parse(line)
    return any(test(token) for token in regular) and not any(test(token) for token in hypernet)

assert(test("abba") == True)
assert(test("abbc") == False)
assert(test("abcd") == False)
assert(test("aaaa") == False)

if __name__ == "__main__":
    print sum(1 for line in fileinput.input() if run(line.strip()))
