#!/usr/bin/python

import fileinput, re, itertools

swap_position = re.compile("swap position (\d+) with position (\d+)")
swap_letter = re.compile("swap letter (\w+) with letter (\w+)")
rotate_left = re.compile("rotate left (\d+)")
rotate_right = re.compile("rotate right (\d+)")
rotate_by_position = re.compile("rotate based on position of letter (\w+)")
reverse_positions = re.compile("reverse positions (\d+) through (\d+)")
move_position = re.compile("move position (\d+) to position (\d+)")

def parse(line, input):
    input = list(input)
    for _x, _y in swap_position.findall(line):
        x, y = int(_x), int(_y)
        input[x], input[y] = input[y], input[x]
    for _x, _y in swap_letter.findall(line):
        x, y = input.index(_x), input.index(_y)
        input[x], input[y] = input[y], input[x]
    for _x in rotate_left.findall(line):
        x = int(_x)
        input = input[x:] + input[:x]
    for _x in rotate_right.findall(line):
        x = int(_x)
        input = input[-x:] + input[:-x]
    for _x in rotate_by_position.findall(line):
        x = input.index(_x)
        x = x + 1 if x < 4 else x + 2 # 4 is an odd rule here
        x = x % len(input)
        input = input[-x:] + input[:-x]
    for _x, _y in reverse_positions.findall(line):
        x, y = int(_x), int(_y)
        input[x:y+1] = input[x:y+1][::-1]
    for _x, _y in move_position.findall(line):
        x, y = int(_x), int(_y)
        _z = input.pop(x)
        input.insert(y, _z)
    return "".join(input)

assert(parse("swap position 4 with position 0", "abcde") == "ebcda")
assert(parse("swap letter d with letter b", "ebcda") == "edcba")
assert(parse("reverse positions 0 through 4", "edcba") == "abcde")
assert(parse("rotate left 1 step", "abcde") == "bcdea")
assert(parse("move position 1 to position 4", "bcdea") == "bdeac")
assert(parse("move position 3 to position 0", "bdeac") == "abdec")
assert(parse("rotate based on position of letter b", "abdec") == "ecabd")
assert(parse("rotate based on position of letter d", "ecabd") == "decab")

if __name__ == "__main__":
    lines = [line.strip() for line in fileinput.input()]
    input = "abcdefgh"
    for line in lines:
        input = parse(line, input)
    print "Part 1:", input
    password = "fbgdceah"
    # Brute force test
    # Literally, the worst way to do this
    for possible in itertools.permutations(password):
        input = possible  
        for line in lines:
            input = parse(line, input)
        if input == password:
            print "Part 2:", ''.join(possible)


