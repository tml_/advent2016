#!/usr/bin/python

import collections, md5

Solution = collections.namedtuple('Solution', ['x', 'y', 'input', 'hash'])

def add_step(solution, dx, dy, step):
    new_hash = solution.hash.copy()
    new_hash.update(step)
    return Solution(solution.x + dx, solution.y + dy, solution.input + step, new_hash)

def shortest_path(input):
    try:
        return all_solutions(input).next()
    except:
        return None

def longest_path_length(input):
    return max(len(path) for path in all_solutions(input))

def all_solutions(input):
    solutions = collections.deque([Solution(0, 0, input, md5.md5(input))])
    while len(solutions) > 0:
        solution = solutions.popleft()
        if solution.x == 3 and solution.y == 3:
            yield solution.input[len(input):]
        else:
            hash = solution.hash.hexdigest()
            if hash[0] in "bcdef" and solution.y > 0: solutions.append(add_step(solution, +0, -1, "U"))
            if hash[1] in "bcdef" and solution.y < 3: solutions.append(add_step(solution, +0, +1, "D"))
            if hash[2] in "bcdef" and solution.x > 0: solutions.append(add_step(solution, -1, +0, "L"))
            if hash[3] in "bcdef" and solution.x < 3: solutions.append(add_step(solution, +1, +0, "R"))

assert(shortest_path("hijkl") == None)
assert(shortest_path("ihgpwlah") == "DDRRRD")
assert(shortest_path("kglvqrro") == "DDUDRLRRUDRD")
assert(shortest_path("ulqzkmiv") == "DRURDRUDDLLDLUURRDULRLDUUDDDRR")
assert(longest_path_length("ihgpwlah") == 370)
assert(longest_path_length("kglvqrro") == 492)
assert(longest_path_length("ulqzkmiv") == 830)

if __name__ == "__main__":
    puzzle = "njfxhljp"
    print "Part 1:", shortest_path(puzzle)
    print "Part 2:", longest_path_length(puzzle)


