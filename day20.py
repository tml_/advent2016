#!/usr/bin/python

import fileinput

def parse(line):
    return map(int, line.split("-"))

def allowed(segments, max):
    sortedlist = sorted(segments)
    minimum = 0
    for low, high in sortedlist:
        while minimum < low:
            yield minimum
            minimum += 1
        if minimum <= high:
            minimum = high + 1
    while minimum <= max:
        yield minimum
        minimum += 1


if __name__ == "__main__":
    segments = []
    for line in fileinput.input():
        segments.append(parse(line.strip()))
    print "Part 1:", allowed(segments, 4294967295).next()
    print "Part 2:", len([ip for ip in allowed(segments, 4294967295)])


