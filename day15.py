#!/usr/bin/python

import fileinput, re

disc_re = re.compile("Disc #\d+ has (\d+) positions; at time=0, it is at position (\d+).")
def disc(line):
    for total, current in disc_re.findall(line):
        return (int(total), int(current))
    raise Exception("Failed parse of line " + line)

def advance(disc, time):
    return (disc[0], (disc[1]+time) % disc[0])

def align(discs):
    time = 0
    while any(advance(discs[i], time+i+1)[1] != 0 for i in range(len(discs))):
        time += 1
    return time

if __name__ == "__main__":
    discs = [disc(line.strip()) for line in fileinput.input()]
    print "Part 1:", align(discs)
    print "Part 2:", align(discs + [(11, 0)])


