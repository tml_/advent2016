#!/usr/bin/python

import fileinput

def decrypt_v2(s):
    out = ""
    pos = 0
    state = ""
    reg = ""
    value1 = 0
    value2 = 0
    while pos < len(s):
        if s[pos] == "(" and state == "":
            state = "("
            pos += 1
        elif s[pos] == "x" and state == "(":
            value1 = int(reg)
            reg = ""
            pos += 1
            state = "x"
        elif s[pos] == ")" and state == "x":
            value2 = int(reg)
            reg = ""
            pos += 1
            out += decrypt_v2(s[pos:pos+value1]) * value2
            pos += value1
            state = ""
        elif state == "(" or state == "x":
            reg += s[pos]
            pos += 1
        elif state == "":
            out += s[pos]
            pos += 1
        else:
            print "Error parsing:", s, pos, state, s[pos]
            assert(False)
    return out

assert(decrypt_v2("ADVENT") == "ADVENT")
assert(decrypt_v2("A(1x5)BC") == "ABBBBBC")
assert(decrypt_v2("(3x3)XYZ") == "XYZXYZXYZ")
assert(decrypt_v2("X(8x2)(3x3)ABCY") == "XABCABCABCABCABCABCY")
assert(len(decrypt_v2("(27x12)(20x12)(13x14)(7x10)(1x12)A")) == 241920)
assert(len(decrypt_v2("(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN")) == 445)

if __name__ == "__main__":
    print sum(len(decrypt_v2(line.strip())) for line in fileinput.input())
