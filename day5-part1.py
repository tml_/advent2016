#!/usr/bin/python

import fileinput
import md5

def run(s):
    count = 0
    result = ""
    while len(result) < 8:
        count += 1
        digest = md5.md5(s + str(count)).hexdigest()
        if digest[:5] == "00000":
            result += digest[5]
    return result

if __name__ == "__main__":
    for line in fileinput.input():
        print run(line.strip())
