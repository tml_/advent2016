#!/usr/bin/python

import fileinput, re, sys

model = {"bot":[], "rules":[], "output":[]}
watch = [17, 61] # Part 1 - Use your values here
outputs = [0, 1, 2] # Part 2 - Use your values here

def push_value(target, index, value):
    for i in range(len(model[target]), index+1):
        model[target] += [[]]
    model[target][index] += [value]
    if target == "bot" and len(model[target][index]) == 2:
        apply_bot_rule(index)

value_to_bot_re = re.compile("value (\d+) goes to bot (\d+)")
def value_to_bot(value, bot):
    push_value("bot", bot, value)

add_bot_rule_re = re.compile("bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)")
def add_bot_rule(bot, low_target, low_index, high_target, high_index):
    push_value("rules", bot, (low_target, low_index, high_target, high_index))

def apply_bot_rule(bot):
    assert(len(model["rules"][bot]) > 0)
    for rule in model["rules"][bot]:
        low_target, low_index, high_target, high_index = rule
        low = min(model["bot"][bot])
        high = max(model["bot"][bot])
        if watch == [low, high]:
            print "bot %s -> sends the value-%s chip to %s %s and gives the value-%s chip to %s %s" % (bot, low, low_target, low_index, high, high_target, high_index)
        push_value(low_target, low_index, low)
        push_value(high_target, high_index, high)
        model["bot"][bot] = []


def eval(line):
    if value_to_bot_re.match(line):
        for value in value_to_bot_re.findall(line):
            value_to_bot(int(value[0]), int(value[1]))
    elif add_bot_rule_re.match(line):
        for value in add_bot_rule_re.findall(line):
            add_bot_rule(int(value[0]), value[1], int(value[2]), value[3], int(value[4]))
    else:
        print "Invalid command: " + line
        assert(False)

if __name__ == "__main__":
    for line in sorted(fileinput.input()):
        eval(line.strip())
    # Part 2
    print "Product of outputs", outputs, reduce(lambda x, y: x*y, (model["output"][output][0] for output in outputs))
