#!/usr/bin/python

import fileinput, re, collections

Node = collections.namedtuple('Node', ['x', 'y', 'size', 'used', 'available', 'used_pct'])

fileinfo_re = re.compile("/dev/grid/node-x(\d+)-y(\d+).*?(\d+)T.*?(\d+)T.*?(\d+)T.*?(\d+)%")
def parse(line): return fileinfo_re.findall(line)

def find_pairs(nodes):
    for i in range(len(nodes)):
        for j in range(len(nodes)):
            if nodes[i].used != 0 and nodes[i].used <= nodes[j].available and i != j:
                yield (nodes[i], nodes[j])

def make_grid(nodes):
    empty = max(node.available for node in nodes)
    grid = {}
    for node in nodes:
        if node.used == 0: grid[(node.x, node.y)] = "_"
        elif node.used > empty: grid[(node.x, node.y)] = "#"
        else: grid[(node.x, node.y)] = "."
    return grid

def print_grid(grid):
    max_x = max(node[0] for node in grid.keys())
    max_y = max(node[1] for node in grid.keys())
    print " ", " ",
    for x in range(0, max_x+1):
        print x / 10,
    print
    print " ", " ", 
    for x in range(0, max_x+1):
        print x % 10,
    print
    for y in range(0, max_y+1):
        print y / 10, y % 10,
        for x in range(0, max_x+1):
            print grid[(x,y)],
        print 

# Get the shortest path to the top right corner
# Then add 5 * width - 1 circles
def allowed_move(grid, path, x, y, max_x, max_y):
    if x < 0 or x > max_x or y < 0 or y > max_y: return False
    if (x, y) in path: return False
    if grid[(x, y)] == "#": return False
    return True

def shortest_path(nodes):
    grid = make_grid(nodes)
    paths = collections.deque([[key for key in grid.keys() if grid[key] == "_"]])
    max_x = max(node[0] for node in grid.keys())
    max_y = max(node[1] for node in grid.keys())
    goal = (max_x, 0)
    while True:
        path = paths.popleft()
        current = path[-1]
        if current == goal: return len(path) - 1 + 5 * (max_x - 1)
        if allowed_move(grid, path, current[0] + 0, current[1] - 1, max_x, max_y): 
            paths.append(path + [(current[0] + 0, current[1] - 1)]) # Up (biased toward this direction)
        else:
            if allowed_move(grid, path, current[0] - 1, current[1] + 0, max_x, max_y): paths.append(path + [(current[0] - 1, current[1] + 0)]) # Left
            if allowed_move(grid, path, current[0] + 1, current[1] + 0, max_x, max_y): paths.append(path + [(current[0] + 1, current[1] + 0)]) # Right
        
                
           
    

if __name__ == "__main__":
    nodes = []
    for line in fileinput.input():
        for node_data in parse(line.strip()):
            node = Node(*map(int, node_data))
            nodes.append(node)
    print "Part 1: ", len([pair for pair in find_pairs(nodes)])
    print "Part 2: ", shortest_path(nodes)
    #print_grid(make_grid(nodes))


