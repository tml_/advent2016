#!/usr/bin/python

import fileinput

def parse(s): return int(s[0:5]), int(s[5:10]), int(s[10:15])
def is_triangle(a, b, c): return a+b > c and b+c > a and a+c > b

if __name__ == "__main__":
    count = 0
    queue = []
    for line in fileinput.input():
        queue += parse(line)
        if len(queue) == 9:
            if is_triangle(queue[0], queue[3], queue[6]): count += 1
            if is_triangle(queue[1], queue[4], queue[7]): count += 1
            if is_triangle(queue[2], queue[5], queue[8]): count += 1
            queue = []
    print count
        