#!/usr/bin/python

import fileinput

def check(s):
    a, b, c = int(s[0:5]), int(s[5:10]), int(s[10:15])
    return a+b > c and b+c > a and a+c > b

if __name__ == "__main__":
    print sum(1 for line in fileinput.input() if check(line))
        