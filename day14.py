#!/usr/bin/python

import sys, md5

def same(l):
    if len(l) == 0: return False
    first = l[0]
    for i in l:
        if first != i: return False
    return True

def streak(s, n):
    for i in range(len(s)-n+1):
        if same(s[i:i+n]):
            return [s[i]] #First only
    return []

saved = []
def digest(s, n, rounds):
    if len(saved) < n+1:
        content = s + str(n)
        for _ in range(rounds+1):
            content = md5.md5(content).hexdigest()
        saved.append(content)
    return saved[n]

def run(s, n, rounds):
    saved = []
    result = []
    count = 0
    while len(result) < n:
        for match in streak(digest(s, count, rounds), 3):
            for i in range(count+1, count+1001):
                if match in streak(digest(s, i, rounds), 5):
                    if count not in result:
                        print count
                        result.append(count)
        count += 1
    return result[-1]

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "USAGE %s input count" % (sys.argv[0])
    else:
        input, count = sys.argv[1], int(sys.argv[2])
        #print "Part 1:", run(input, count, 0)
        print "Part 2:", run(input, count, 2016)

