#!/usr/bin/python

import fileinput

mapper = {
    "U":{"1":"1", "2":"2", "3":"1", "4":"4", "5":"5", "6":"2", "7":"3", "8":"4", "9":"9", "A":"6", "B":"7", "C":"8", "D":"B"},
    "L":{"1":"1", "2":"2", "3":"2", "4":"3", "5":"5", "6":"5", "7":"6", "8":"7", "9":"8", "A":"A", "B":"A", "C":"B", "D":"D"},
    "R":{"1":"1", "2":"3", "3":"4", "4":"4", "5":"6", "6":"7", "7":"8", "8":"9", "9":"9", "A":"B", "B":"C", "C":"C", "D":"D"},
    "D":{"1":"3", "2":"6", "3":"7", "4":"8", "5":"5", "6":"A", "7":"B", "8":"C", "9":"9", "A":"A", "B":"D", "C":"C", "D":"D"}
}


def run(position, codes):
    for code in codes:
        if code in mapper:
            position = mapper[code][position]
    return position
    


if __name__ == "__main__":
    result = ""
    digit = "5"
    for line in fileinput.input():
        digit = run(digit, line)
        result += digit
    print result
        