#!/usr/bin/python

import fileinput
import re

def parse(s): 
    return re.findall('([a-z-]+)([0-9]+)[[]([a-z]+)[]]', s)[0]

def checksum(name):
    count = {}
    for letter in name.replace("-", ""):
        if letter in count:
            count[letter] += 1
        else:
            count[letter] = 1
    # Order by count then alphabetically
    ordered = sorted(count.items(), cmp=lambda x, y: cmp(y[1], x[1]) if cmp(y[1], x[1]) != 0 else cmp(x[0], y[0]))
    # Get first 5 letters
    return "".join([l[0] for l in ordered][:5]) 

if __name__ == "__main__":
    total = 0
    for line in fileinput.input():
        obj = parse(line)
        if obj[2] == checksum(obj[0]):
            total += int(obj[1])
    print total
        