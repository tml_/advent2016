#!/usr/bin/python

def tile(left, center, right):
    if left == '^' and center == '^' and right == '.': return '^'
    if left == '.' and center == '^' and right == '^': return '^'
    if left == '^' and center == '.' and right == '.': return '^'
    if left == '.' and center == '.' and right == '^': return '^'
    return '.'

def new_row(row):
    padded_row = "." + row + "."
    return "".join(tile(*padded_row[i:i+3]) for i in range(len(padded_row)-2))

assert(new_row("..^^.") == ".^^^^")
assert(new_row(".^^^^") == "^^..^")

def generate_maze(start_row, count):
    assert(count > 0)
    row = start_row
    maze = row
    for i in range(count-1):
        row = new_row(row)
        maze += "\n" + row
    return maze

sample_maze = """.^^.^.^^^^
^^^...^..^
^.^^.^.^^.
..^^...^^^
.^^^^.^^.^
^^..^.^^..
^^^^..^^^.
^..^^^^.^^
.^^^..^.^^
^^.^^^..^^"""
assert(generate_maze(".^^.^.^^^^", 10) == sample_maze)

def count_safe_tiles(maze):
    return sum(1 for tile in maze if tile == ".")

assert(count_safe_tiles(generate_maze(".^^.^.^^^^", 10)) == 38)

if __name__ == "__main__":
    puzzle = "^.^^^.^..^....^^....^^^^.^^.^...^^.^.^^.^^.^^..^.^...^.^..^.^^.^..^.....^^^.^.^^^..^^...^^^...^...^."
    print "Part 1:", count_safe_tiles(generate_maze(puzzle, 40))
    print "Part 2:", count_safe_tiles(generate_maze(puzzle, 400000))


