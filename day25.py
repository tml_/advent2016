#!/usr/bin/python

import fileinput

def parse(line):
    codes = line.split(" ")
    if len(codes) < 3:
        codes += [None] * (3 - len(codes))
    return codes

def tgl(x):
    if x == "tgl": return "inc"
    if x == "jnz": return "cpy"
    if x == "cpy": return "jnz"
    if x == "inc": return "dec"
    if x == "dec": return "inc"
    if x == "add": assert(False)
    if x == "mul": assert(False)

def eval(val, reg):
    if val in reg:
        return reg[val]
    else:
        return int(val)

def apply(program, reg):
    ptr = reg["ptr"] * 3
    ins = program[ptr:ptr+3]
    if ins[0] == "tgl":
        tgl_ptr = ptr + eval(ins[1], reg) * 3
        print tgl_ptr
        if tgl_ptr < len(program):
            program[tgl_ptr] = tgl(program[tgl_ptr])
        reg["ptr"] += 1
    elif ins[0] == "cpy":
        reg[ins[2]] = eval(ins[1], reg)
        reg["ptr"] += 1
    elif ins[0] == "inc":
        reg[ins[1]] += 1
        reg["ptr"] += 1
    elif ins[0] == "dec":
        reg[ins[1]] -= 1
        reg["ptr"] += 1
    elif ins[0] == "jnz":
        if eval(ins[1], reg) != 0:
            reg["ptr"] += eval(ins[2], reg)
        else:
            reg["ptr"] += 1
    elif ins[0] == "add":
        reg[ins[1]] += eval(ins[2], reg)
        reg[ins[2]] = 0
        reg["ptr"] += 1
    elif ins[0] == "mul":
        reg[ins[1]] *= eval(ins[2], reg)
        reg[ins[2]] = 0
        reg["ptr"] += 1
    elif ins[0] == "out":
        reg["out"] += str(eval(ins[1], reg))
        if len(reg["out"]) >= 8: reg["ins"] = 99999
        reg["ptr"] += 1
    reg["ins"] += 1

def execute(program, reg, stop=99999):
    while reg["ptr"] * 3 < len(program) and reg["ins"] < stop:
        apply(program, reg)
    return reg

def search(program, pattern):
    x = 0
    while True:
        reg = execute(program[:], {"a": x, "b": 0, "c": 0, "d": 0, "ptr": 0, "ins": 0, "out": ""})
        print reg
        if reg["out"] == pattern:
            return x
        x += 1

if __name__ == "__main__":
    program = []
    for line in fileinput.input():
        program += parse(line.strip())
    print "Part 1:", search(program[:], "01010101")
    print "Part 2:", None
