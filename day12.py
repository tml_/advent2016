#!/usr/bin/python

import fileinput

def parse(line):
    codes = line.split(" ")
    if len(codes) < 3:
        codes += [None] * (3 - len(codes))
    return codes

def eval(val, reg):
    if val in reg:
        return reg[val]
    else:
        return int(val)

def apply(ins, reg):
    if ins[0] == "cpy":
        reg[ins[2]] = eval(ins[1], reg)
        reg["ptr"] += 1
    if ins[0] == "inc":
        reg[ins[1]] += 1
        reg["ptr"] += 1
    if ins[0] == "dec":
        reg[ins[1]] -= 1
        reg["ptr"] += 1
    if ins[0] == "jnz":
        if eval(ins[1], reg) != 0:
            reg["ptr"] += eval(ins[2], reg)
        else:
            reg["ptr"] += 1
    reg["ins"] += 1

def execute(program, reg):
    while reg["ptr"] * 3 < len(program):
        ptr = reg["ptr"] * 3
        apply(program[ptr:ptr+3], reg)
    return reg

if __name__ == "__main__":
    program = []
    for line in fileinput.input():
        program += parse(line.strip())
    print "Part 1:", execute(program, {"a": 0, "b": 0, "c": 0, "d": 0, "ptr": 0, "ins": 0})
    print "Part 2:", execute(program, {"a": 0, "b": 0, "c": 1, "d": 0, "ptr": 0, "ins": 0})
