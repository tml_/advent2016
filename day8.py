#!/usr/bin/python

import fileinput
import re

w = 50
h = 6
screen = [0] * (w * h)

def index(x,y):
    return x + y * w

rect_re = re.compile("rect ([0-9]+)x([0-9]+)")
def rect(A,B):
    for x in range(A):
        for y in range(B):
            screen[index(x,y)] = 1

rotate_x_re = re.compile("rotate column x=([0-9]+) by ([0-9]+)")
def rotate_column_x(A,B):
    column = [screen[index(A,i)] for i in range(h)]
    rotated = column[(h-B):] + column[:(h-B)]
    for i in range(h):
        screen[index(A,i)] = rotated[i]

rotate_y_re = re.compile("rotate row y=([0-9]+) by ([0-9]+)")
def rotate_row_y(A,B):
    row = [screen[index(i,A)] for i in range(w)]
    rotated = row[(w-B):] + row[:(w-B)]
    for i in range(w):
        screen[index(i,A)] = rotated[i]

def show_grid():
    for i in range(h):
        for j in range(w):
            print "#" if screen[index(j,i)] == 1 else ".",
        print
    print

def eval(line):
    if rect_re.match(line):
        for value in rect_re.findall(line):
            rect(int(value[0]), int(value[1]))
    elif rotate_x_re.match(line):
        for value in rotate_x_re.findall(line):
            rotate_column_x(int(value[0]), int(value[1]))
    elif rotate_y_re.match(line):
        for value in rotate_y_re.findall(line):
            rotate_row_y(int(value[0]), int(value[1]))
    else:
        print "Invalid command: " + line
        assert(False)


if __name__ == "__main__":
    for line in fileinput.input():
        eval(line)
    show_grid()
    print sum(screen)
