#!/usr/bin/python

import fileinput
import md5

def run(s):
    count = 0
    result = "________"
    remain = 8
    while remain > 0:
        count += 1
        digest = md5.md5(s + str(count)).hexdigest()
        if digest[:5] == "00000":
            place = digest[5]
            if place in "01234567":
                if result[int(place)] == "_":
                    result = result[:int(place)] + digest[6] + result[int(place)+1:]
                    remain -= 1
                    print result, count, digest
    return result

if __name__ == "__main__":
    for line in fileinput.input():
        print run(line.strip())
