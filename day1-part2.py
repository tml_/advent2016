#!/usr/bin/python

import fileinput

left = {"N":"W", "E":"N", "S":"E", "W":"S"}
right = {"N":"E", "E":"S", "S":"W", "W":"N"}
vectors = {"N":(0,1), "E":(1,0), "S":(0,-1), "W":(-1,0)}

def run(s, visited, direction):
    codes = s.split(", ")
    for code in codes:
        if code[0] == "L":
            direction = left[direction]
        elif code[0] == "R":
            direction = right[direction]
        else:
            print "ERROR: Unhandled %s" % (s)
        for i in range(int(code[1:])):
            new_position = [visited[-1][0]+vectors[direction][0], visited[-1][1]+vectors[direction][1]]
            if new_position in visited:
                return abs(new_position[0]) + abs(new_position[1])
            else:
                visited += [new_position]


if __name__ == "__main__":
    for line in fileinput.input():
        print run(line, [[0,0]], "N")
        