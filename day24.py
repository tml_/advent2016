#!/usr/bin/python

import sys, collections, itertools

Maze = collections.namedtuple("Maze", ["grid", "points", "graph"])

def parse_maze(input):
    x, y = 0, 0
    maze = Maze({}, {}, {})
    for letter in input:
        if letter == "\n":
            x, y = 0, y+1
        elif letter == "." or letter == "#":
            maze.grid[(x,y)] = letter
            x, y = x+1, y
        else:
            maze.points[letter] = (x,y)
            maze.grid[(x,y)] = "."
            x, y = x+1, y
    for start in maze.points.keys():
        for end in maze.points.keys():
            shortest_path = find_path_length(maze, start, end)
            maze.graph[(start, end)] = shortest_path
            maze.graph[(end, start)] = shortest_path
    return maze

def find_path_length(maze, a, b):
    paths = collections.deque([[maze.points[a]]])
    visited = set([maze.points[a]])
    while True:
        current = paths.popleft()
        end = current[-1]
        if end == maze.points[b]:
            return len(current) - 1
        else:
            for direction in [(end[0], end[1]-1), (end[0], end[1]+1), (end[0]-1, end[1]), (end[0]+1, end[1])]:
                if maze.grid[direction] == "." and direction not in visited: # No walls, no backtracking
                    visited.add(direction)
                    paths.append(current + [direction])

def route_length(maze, route):
    total = 0
    for i in range(1, len(route)):
        total += maze.graph[(route[i-1], route[i])]
    return total

def find_routes(maze):
    points = [point for point in maze.points.keys() if point != "0"]
    return [list(route) for route in itertools.permutations(points)]

if __name__ == "__main__":
    maze = parse_maze(sys.stdin.read())
    all_routes = find_routes(maze)
    print "Part 1:", min(map(lambda x: route_length(maze, x), map(lambda x: ["0"] + x, all_routes)))
    print "Part 2:", min(map(lambda x: route_length(maze, x), map(lambda x: ["0"] + x + ["0"], all_routes)))
