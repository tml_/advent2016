#!/usr/bin/python

import fileinput

def decrypt(s):
    out = ""
    pos = 0
    state = ""
    reg = ""
    value1 = 0
    value2 = 0
    while pos < len(s):
        if s[pos] == "(" and state == "":
            state = "("
            pos += 1
        elif s[pos] == "x" and state == "(":
            value1 = int(reg)
            reg = ""
            pos += 1
            state = "x"
        elif s[pos] == ")" and state == "x":
            value2 = int(reg)
            reg = ""
            pos += 1
            out += s[pos:pos+value1] * value2
            pos += value1
            state = ""
        elif state == "(" or state == "x":
            reg += s[pos]
            pos += 1
        elif state == "":
            out += s[pos]
            pos += 1
        else:
            print "Error parsing:", s, pos, state, s[pos]
            assert(False)
    return out

assert(decrypt("ADVENT") == "ADVENT")
assert(decrypt("A(1x5)BC") == "ABBBBBC")
assert(decrypt("(3x3)XYZ") == "XYZXYZXYZ")
assert(decrypt("A(2x2)BCD(2x2)EFG") == "ABCBCDEFEFG")
assert(decrypt("(6x1)(1x3)A") == "(1x3)A")
assert(decrypt("X(8x2)(3x3)ABCY") == "X(3x3)ABC(3x3)ABCY")

if __name__ == "__main__":
    print sum(len(decrypt(line.strip())) for line in fileinput.input())
