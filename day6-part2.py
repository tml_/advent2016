#!/usr/bin/python

import fileinput

def highest_key(d):
    if len(d) == 0: return None
    return sorted(d.keys(), cmp=lambda x, y: cmp(d[x], d[y]))[0]

if __name__ == "__main__":
    model = []
    for line in fileinput.input():
        for i in range(len(line)):
            if i+1 > len(model):
                model += [{}]
            if line[i] not in model[i]:
                model[i][line[i]] = 1
            else:
                model[i][line[i]] += 1
    print "".join(highest_key(d) for d in model)
        
